# Tic Tac Toe in Rust

Command Line Tic Tac Toe written in Rust

## Game Modes

* Human Versus Human
* Human Versus Computer
* Computer Versus Computer

## Setup Rust

Make sure you have Rust installed on your local machine. You can find the instructions for installing Rust on [rust-lang.org](https://www.rust-lang.org/en-US/install.html).
To confirm proper installation run the following command after installation:
```
$ rustc --version
```

You should see something like this:
```
rustc 1.27.1 (5f2b325f6 2018-07-07)
```

## Install dependencies

Pull down this project and navigate to the root directory:
```
$ git clone https://gitlab.com/softcircles/rust-ttt.git
$ cd rust_ttt/
```

Once in the root directory install the dependencies with Cargo (Cargo comes bundled when you install Rust)
```
$ cargo build
```
Cargo grabs our dependencies and then builds the binary file so we can execute the program.

## Running the application

The binary file gets built in ```./target/debug/rust_ttt``` so we will execute the program from that location.

To play a basic Human v. Human game run the following command with the following options:
```
$ ./target/debug/rust_ttt --HVH 3 X
```

## Game Modes

To get help with any of the commands or options enter the following command:
```
$ ./target/debug/rust_ttt --help
```

#### Options for Human Versus Human

Human v. Human takes options for ```<row size>``` and the ```<first player symbol>```.


```
--HVH <row size> <first player symbol>
```

```<row size>``` can range from a minimum of ```3``` to a maximum of ```10```.

```<first player symbol>``` must be either ```X``` or ```O```.

Example:
```
$ ./target/debug/rust_ttt --HVH 5 O
```
This command will start a Human v. Human game with a ```<row size>``` of ```5``` and the first player will start with an ```O``` marker.

#### Options for Human Versus Computer

Human v. Computer takes options for ```<row size>```, ```<first player symbol>``` and who the ```<first player>``` should be, computer or human.
```
--HVC <row size> <first player symbol> <first player>
```
```row size``` can range from a minimum of ```3``` to a maximum of ```4```.

```<first player symbol>``` must be either ```X``` or ```O```.

Indicate you want to be ```<first player>``` by entering ```H```. If you want the computer to go first enter ```C```.

Example:

```
$ ./target/debug/rust_ttt --HVC 3 X C
```
This command will start a Human v. Computer game with a ```<row size>``` of ```3``` . The ```<first player symbol>``` will be ```X``` and the Computer will be the ```<first player>```.

### Options for Computer Versus Computer

Computer v. Computer takes options for ```<row size>``` and ```<first player symbol>```.

```
--CVC <row size> <first player symbol>
```
```row size``` can range from a minimum of ```3``` to a maximum of ```4```.

```<first player symbol>``` must be either ```X``` or ```O```.

Example:

```
$ ./target/debug/rust_ttt --CVC 4 O
```

This command will start a Computer v. Computer game with a ```<row size>``` of ```4```. The ```<first player symbol>``` will be ```O```.

## Tests

To run the test suite enter the following command:
```
$ cargo test
```
