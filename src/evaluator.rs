use board::Board;
use rules;
use marker::Marker;
use game_state::GameState;

pub enum Evaluator {
    Worst,
    Tie,
    Best,
}

impl Evaluator {

    fn value(&self) -> i32 {
        match self {
            Evaluator::Worst => -1000,
            Evaluator::Best => 1000,
            Evaluator::Tie => 0
        }
    }

    pub fn score(board: &Board, computer: Marker, depth: i32) -> Option<i32> {
        match rules::get_game_state(board) {
            GameState::Won => match rules::get_winner(board) {
                Some(winner) => {
                    if winner == computer {
                        Some(Evaluator::Best.value() / depth)
                    } else {
                        Some(Evaluator::Worst.value() / depth)
                    }
                },
                None => Some(Evaluator::Worst.value() / depth)
            },
            GameState::Tie => Some(Evaluator::Tie.value()),
            GameState::NotOver => Some(Evaluator::Worst.value() / depth),
            GameState::Error => None
        }
    }

}

#[cfg(test)]
mod tests {

    use evaluator::Evaluator;
    use board::Board;
    use marker::Marker;

    const ROW_SIZE: usize = 3;
    const DEPTH: i32 = 6;

    #[test]
    fn returns_positive_score_if_current_player_wins() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::O, Marker::X
        ];
        assert_eq!(Evaluator::score(&board, Marker::X, DEPTH).unwrap(), 166);
    }

    #[test]
    fn returns_negative_score_if_current_player_loses() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::O, Marker::X
        ];
        assert_eq!(Evaluator::score(&board, Marker::O, DEPTH).unwrap(), -166);
    }

    #[test]
    fn returns_negative_score_if_current_player_loses1() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::X,
            Marker::O, Marker::X, Marker::O
        ];
        assert_eq!(Evaluator::score(&board, Marker::X, DEPTH).unwrap(), -166);
    }

    #[test]
    fn returns_zero_score_if_current_player_ties() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::X,
            Marker::X, Marker::O, Marker::X
        ];
        assert_eq!(Evaluator::score(&board, Marker::X, DEPTH).unwrap(), 0);
    }

    #[test]
    fn returns_zero_score_if_current_player_ties1() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::X,
            Marker::X, Marker::O, Marker::X
        ];
        assert_eq!(Evaluator::score(&board, Marker::O, DEPTH).unwrap(), 0);
    }

    #[test]
    fn returns_expected_value_for_best_eval() {
        assert_eq!(1000, Evaluator::Best.value());
    }

    #[test]
    fn returns_expected_value_for_worst_eval() {
        assert_eq!(-1000, Evaluator::Worst.value());
    }

    #[test]
    fn returns_expected_value_for_tie_eval() {
        assert_eq!(0, Evaluator::Tie.value());
    }
}
