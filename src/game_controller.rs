use board::Board;
use messages;
use player::Player;
use rules;

pub struct GameController {
    board: Board,
    players: Vec<Box<dyn Player>>,
}

impl GameController {
    pub fn new(players: Vec<Box<dyn Player>>, board: Board) -> GameController {
        GameController { players, board }
    }

    pub fn play(&mut self) {
        self.take_turns();
        println!("{}", messages::display_board(&self.board));
        println!("{}", messages::end_game(&self.board));
    }

    fn take_turns(&mut self) {
        while !rules::game_over(&self.board) {
            for player in self.players.iter() {
                println!("{}", messages::display_board(&self.board));
                self.board = player.take_turn(&self.board);
                if rules::game_over(&self.board) {
                    break;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {

    use board::Board;
    use computer::Computer;
    use game_controller::GameController;
    use human::Human;
    use marker::Marker;

    const ROW_SIZE: usize = 3;

    #[test]
    fn creates_player_pool() {
        let player1 = Human::new(Marker::X);
        let player2 = Computer::new(Marker::O);
        let board = Board::new(ROW_SIZE);
        let game = GameController::new(vec![Box::new(player1), Box::new(player2)], board);
        assert_eq!(2, game.players.len());
    }
}
