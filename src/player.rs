use board::Board;
pub trait Player{
    fn take_turn(&self, board: &Board) -> Board;
}
